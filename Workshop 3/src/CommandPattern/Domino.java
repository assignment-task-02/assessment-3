package CommandPattern;

public class Domino {
    private int value;

    public Domino(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
