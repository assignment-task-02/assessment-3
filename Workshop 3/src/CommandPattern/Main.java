package CommandPattern;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Create a list of dominoes
        List<Domino> dominoes = new ArrayList<>();
        dominoes.add(new Domino(1));
        dominoes.add(new Domino(2));
        dominoes.add(new Domino(3));
        dominoes.add(new Domino(4));
        dominoes.add(new Domino(5));

        // Create a data object with the list of dominoes
        Data data = new Data(dominoes);

        // Create the shuffle command
        Command shuffleCommand = new ShuffleDominoesCommand(data);

        // Execute the shuffle command
        shuffleCommand.execute();

        // Print the shuffled dominoes
        System.out.println("Shuffled Dominoes: " + data._d);

        // Undo the shuffle
        shuffleCommand.undo();

        // Print the original dominoes
        System.out.println("Original Dominoes: " + data._d);
    }
}