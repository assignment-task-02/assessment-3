package CommandPattern;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ShuffleDominoesCommand implements Command {
    private final List<Domino> shuffled;
    private final List<Domino> original;
    private final Data data;

    public ShuffleDominoesCommand(Data data) {
        this.data = data;
        this.original = new ArrayList<>(data._d);
        this.shuffled = new LinkedList<>();
    }

    @Override
    public void execute() {
        System.out.println("Shuffling dominoes...");

        while (data._d.size() > 0) {
            int n = (int) (Math.random() * data._d.size());
            shuffled.add(data._d.get(n));
            data._d.remove(n);
        }

        data._d = shuffled;

        System.out.println("Dominoes shuffled.");
    }

    @Override
    public void undo() {
        System.out.println("Undoing shuffle...");

        data._d = original;

        System.out.println("Shuffle undone.");
    }
}