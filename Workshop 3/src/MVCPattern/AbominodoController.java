package MVCPattern;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AbominodoController {
    private AbominodoModel model;
    private AbominodoView view;
    private MainMenuView mainMenuView;

    public AbominodoController(AbominodoModel model, AbominodoView view) {
        this.model = model;
        this.view = view;
        this.view.setStartGameButtonListener(new StartGameButtonListener());
    }

    private class StartGameButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String playerName = view.getPlayerName();
            model.setPlayerName(playerName);

            // Hide the current view
            view.show(false);

            // Create and show the main menu view
            mainMenuView = new MainMenuView(playerName);
            mainMenuView.setPlayButtonListener(new PlayButtonListener());
            mainMenuView.setViewScoresButtonListener(new ViewScoresButtonListener());
            mainMenuView.setViewRulesButtonListener(new ViewRulesButtonListener());
            mainMenuView.setExitButtonListener(new ExitButtonListener());
        }
    }

    private class PlayButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Handle "Play" button click
        }
    }

    private class ViewScoresButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Handle "View Scores" button click
        }
    }

    private class ViewRulesButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Handle "View Rules" button click
        }
    }

    private class ExitButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}
