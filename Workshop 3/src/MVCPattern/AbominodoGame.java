package MVCPattern;

public class AbominodoGame {
    public void PictureFrame() {
        AbominodoModel model = new AbominodoModel();
        AbominodoView view = new AbominodoView();
        AbominodoController controller = new AbominodoController(model, view);

        view.show();
    }

    public static void main(String[] args) {
        AbominodoGame game = new AbominodoGame();
        game.PictureFrame();
    }
}
