package MVCPattern;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AbominodoView {
    private JFrame frame;
    private JButton startGameButton;
    private JTextField playerNameField;

    public AbominodoView() {
        frame = new JFrame("Abominodo");

        // Create a panel for the welcome message and version information
        JPanel topPanel = new JPanel();
        JLabel welcomeLabel = new JLabel("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe");
        JLabel versionLabel = new JLabel("Version 1.0 (c), Kevan Buckley, 2010");
        topPanel.add(welcomeLabel);
        topPanel.add(versionLabel);

        startGameButton = new JButton("Start Game");
        playerNameField = new JTextField(20);

        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 0, 10, 0); // Add spacing between components

        // Add the welcome message and version information at the top
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(topPanel, gbc);

        // Add "Enter your name" label
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(new JLabel("Enter your name: "), gbc);

        // Add the player name text field
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(playerNameField, gbc);

        // Add the "Start Game" button
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(startGameButton, gbc);

        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        // Add a window listener to handle the window closing event
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // Perform any necessary cleanup or save operations
                // and then close the application
                System.exit(0);
            }
        });
    }

    public void show() {
        frame.setVisible(true);
    }

    public void setStartGameButtonListener(ActionListener listener) {
        startGameButton.addActionListener(listener);
    }

    public String getPlayerName() {
        return playerNameField.getText();
    }
    public void show(boolean visible) {
        frame.setVisible(visible);
    }
}
