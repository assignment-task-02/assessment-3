package MVCPattern;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenuView {
    private JFrame frame;
    private JLabel welcomeLabel;
    private JButton playButton;
    private JButton viewScoresButton;
    private JButton viewRulesButton;
    private JButton exitButton;

    public MainMenuView(String playerName) {
        frame = new JFrame("Abominodo - Main Menu");

        welcomeLabel = new JLabel("Welcome, " + playerName + "!");
        playButton = new JButton("Play");
        viewScoresButton = new JButton("View Scores");
        viewRulesButton = new JButton("View Rules");
        exitButton = new JButton("Exit");

        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 0, 10, 0); // Add spacing between components

        // Add the welcome label
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(welcomeLabel, gbc);

        // Add the "Play" button
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(playButton, gbc);

        // Add the "View Scores" button
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(viewScoresButton, gbc);

        // Add the "View Rules" button
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(viewRulesButton, gbc);

        // Add the "Exit" button
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.CENTER;
        mainPanel.add(exitButton, gbc);

        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void setPlayButtonListener(ActionListener listener) {
        playButton.addActionListener(listener);
    }

    public void setViewScoresButtonListener(ActionListener listener) {
        viewScoresButton.addActionListener(listener);
    }

    public void setViewRulesButtonListener(ActionListener listener) {
        viewRulesButton.addActionListener(listener);
    }

    public void setExitButtonListener(ActionListener listener) {
        exitButton.addActionListener(listener);
    }
}
