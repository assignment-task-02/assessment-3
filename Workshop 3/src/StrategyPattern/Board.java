package StrategyPattern;

import java.util.List;

public class Board {
    private List<Tile> tiles;

    public Board(List<Tile> tiles) {
        this.tiles = tiles;
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public void setTiles(List<Tile> tiles) {
        this.tiles = tiles;
    }

    public int calculateScore(ScoringStrategy scoringStrategy) {
        return scoringStrategy.calculateScore(this);
    }
}
