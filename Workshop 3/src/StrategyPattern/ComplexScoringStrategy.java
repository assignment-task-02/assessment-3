package StrategyPattern;

public class ComplexScoringStrategy implements ScoringStrategy {
    @Override
    public int calculateScore(Board board) {
        int score = 0;
        for (Tile tile : board.getTiles()) {
            score += tile.getValue() * 2; // Apply a bonus multiplier
        }
        return score;
    }
}
