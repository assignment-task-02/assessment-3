package StrategyPattern;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Create tiles
        Tile tile1 = new Tile(5);
        Tile tile2 = new Tile(3);
        Tile tile3 = new Tile(0);
        Tile tile4 = new Tile(2);
        Tile tile5 = new Tile(1);
        Tile tile6 = new Tile(4);
        
        // Create a list of tiles
        List<Tile> tiles = new ArrayList<>();
        tiles.add(tile1);
        tiles.add(tile2);
        tiles.add(tile3);
        tiles.add(tile4);
        tiles.add(tile5);
        tiles.add(tile6);
        
        // Create the board with the tile list
        Board board = new Board(tiles);
        
        // Create scoring strategies
        ScoringStrategy simpleStrategy = new SimpleScoringStrategy();
        ScoringStrategy complexStrategy = new ComplexScoringStrategy();
        ScoringStrategy specialCombinationStrategy = new SpecialCombinationScoringStrategy();
        
        // Calculate scores using different strategies
        int simpleScore = board.calculateScore(simpleStrategy);
        int complexScore = board.calculateScore(complexStrategy);
        int specialCombinationScore = board.calculateScore(specialCombinationStrategy);
        
        // Print scores
        System.out.println("Simple score: " + simpleScore);
        System.out.println("Complex score: " + complexScore);
        System.out.println("Special combination score: " + specialCombinationScore);
    }
}