package StrategyPattern;

public interface ScoringStrategy {
    int calculateScore(Board board);
}
