package StrategyPattern;

public class SimpleScoringStrategy implements ScoringStrategy {
    @Override
    public int calculateScore(Board board) {
        int score = 0;
        for (Tile tile : board.getTiles()) {
            score += tile.getValue();
        }
        return score;
    }
}
