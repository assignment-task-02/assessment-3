package StrategyPattern;

public class SpecialCombinationScoringStrategy implements ScoringStrategy {
    @Override
    public int calculateScore(Board board) {
        int score = 0;
        int consecutiveTiles = 0;
        int consecutiveMultiplier = 1;
        
        for (Tile tile : board.getTiles()) {
            if (tile.getValue() > 0) {
                score += tile.getValue() * consecutiveMultiplier;
                consecutiveTiles++;
                if (consecutiveTiles >= 3) {
                    consecutiveMultiplier++;
                }
            } else {
                consecutiveTiles = 0;
                consecutiveMultiplier = 1;
            }
        }
        
        return score;
    }
}